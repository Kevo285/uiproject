﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public Transform Target;
    public Vector3 offset;
    public float rotateSpeed;
    public bool useOffsetValue;
    public Transform pivot;
    public float maxViewAngle;
    public float minViewAngle;
    public bool invertY;

    // Start is called before the first frame update
    void Start()
    {
        if (!useOffsetValue)
        {
            offset = Target.position - transform.position;
        }

        pivot.transform.position = Target.transform.position;
        pivot.transform.parent = Target.transform;

        Cursor.lockState = CursorLockMode.Locked;

        pivot.transform.parent = null;
    }

    // Update is called once per frame
    void LateUpdate()
    {

        pivot.transform.position = Target.transform.position;

        // get x position of mouse
        float horizontal = Input.GetAxis("Mouse X") * rotateSpeed;
        pivot.Rotate(0, horizontal, 0);

        // get y position of mouse
        float vertical = Input.GetAxis("Mouse Y") * rotateSpeed;
       
        
        if (invertY)
        {
            pivot.Rotate(vertical, 0, 0);
        }
        else
        {
        pivot.Rotate(-vertical, 0, 0);
        }

        if(pivot.rotation.eulerAngles.x > maxViewAngle && pivot.rotation.eulerAngles.x < 180f)
        {

            pivot.rotation = Quaternion.Euler(maxViewAngle, 0, 0);

        }

        if(pivot.rotation.eulerAngles.x > 180f && pivot.rotation.eulerAngles.x < 360f + minViewAngle)
        {
            pivot.rotation = Quaternion.Euler(360f + minViewAngle, 0, 0);
        }

        float desiredYAngle = pivot.eulerAngles.y;
        float desiredXAngle = pivot.eulerAngles.x;

        Quaternion rotation = Quaternion.Euler(desiredXAngle, desiredYAngle, 0);
        transform.position = Target.position - (rotation * offset);
        //transform.position = Target.position - offset;


        if(transform.position.y < Target.position.y)
        {


            transform.position = new Vector3(transform.position.x, Target.position.y -.5f, transform.position.z);


        }







        transform.LookAt(Target);
    }
}
