﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Health : MonoBehaviour
{


    public GameObject thePlayer;
    public Image circleBar;
    public Image extraBar;
    public float currentHealth = 0;
    public float maxHealth = 100;
    //how much of the health bar is the circle
    public float circlePercentage = 0.3f;
    //how much of the cicular part is used in health bar
    private const float circleFillAmount = 0.75f;

    // Update is called once per frame
    void Update()
    {
        CircleFill();
        ExtraFill();

    }


    void CircleFill()
    {

    float healthPercentage = currentHealth / maxHealth;
    float circleFill = healthPercentage / circlePercentage;

        circleFill *= circleFillAmount;
        circleFill = Mathf.Clamp(circleFill, 0, circleFillAmount);

        circleBar.fillAmount = circleFill;
        }


    void ExtraFill()
    {
        float circleAmount = circlePercentage * maxHealth;
        float extraHealth = currentHealth - circleAmount;
        float extraTotalHealth = maxHealth - circleAmount;
        float ExtraFill = extraHealth / extraTotalHealth;

        ExtraFill = Mathf.Clamp(ExtraFill, 0, 1);
        extraBar.fillAmount = ExtraFill;

    }

    
    public void HurtPlayer(int damage)
    {

        currentHealth -= damage;

    }

    public void HealPlayer(int healAmount)
    {

        currentHealth += healAmount;

        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }


    }


}
