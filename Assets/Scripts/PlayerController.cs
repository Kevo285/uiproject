﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    public float moveSpeed;
    //public Rigidbody RB;
    public float jumpForce;
    public CharacterController controller;
    private Vector3 moveDirection;
    public float gravityScale;

    public Animator anim;
   
    public Transform pivot;
    public float rotateSpeed;
 public GameObject playerModel;
    //private int numCoins;
    //public Text numCoinText;
    //public Text Wintext;


    // Start is called before the first frame update
    void Start()
    {

        //RB = GetComponent<Rigidbody>();

        controller = GetComponent<CharacterController>();

        

        
    }

    // Update is called once per frame
    void Update()
    {
        //RB.velocity = new Vector3(Input.GetAxis("Horizontal") * moveSpeed, RB.velocity.y, Input.GetAxis("Vertical") * moveSpeed);

        // if (Input.GetButtonDown("Jump")){

        //RB.velocity = new Vector3(RB.velocity.x, jumpForce, RB.velocity.y);

        float yStore = moveDirection.y;
        moveDirection = (transform.forward * Input.GetAxisRaw("Vertical")) + (transform.right * Input.GetAxisRaw("Horizontal"));
        moveDirection = moveDirection.normalized * moveSpeed;
        moveDirection.y = yStore;

        if (controller.isGrounded)
        {
            moveDirection.y = 0f;
            if (Input.GetButtonDown("Jump"))
            {

                moveDirection.y = jumpForce;

            }
        }
       // if (!controller.isGrounded)
      //  {
       //     moveDirection.y = moveDirection.y + (Physics.gravity.y * gravityScale * Time.deltaTime);
       // }

        moveDirection.y = moveDirection.y + (Physics.gravity.y * gravityScale * Time.deltaTime);

        controller.Move(moveDirection * Time.deltaTime);



        //move play in different direction

        if(Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
        {

            transform.rotation = Quaternion.Euler(0f, pivot.rotation.eulerAngles.y, 0f);
            Quaternion newRotation = Quaternion.LookRotation(new Vector3(moveDirection.x, 0f, moveDirection.z));
            playerModel.transform.rotation = Quaternion.Slerp(playerModel.transform.rotation, newRotation, rotateSpeed * Time.deltaTime);
        }


        anim.SetBool("isGrounded", controller.isGrounded);

        anim.SetFloat("Speed", (Mathf.Abs(Input.GetAxis("Vertical")) + Mathf.Abs(Input.GetAxis("Horizontal"))));





    }




//void setCountText()
   // {
   // numCoinText.text = "Count: " + numCoins.ToString();
           // if (numCoins >= 5)
           // {
            //    Wintext.text = "Congratulations!";
           // }
      //  }


}

 